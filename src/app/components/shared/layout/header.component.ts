import { Component } from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})

export class HeaderComponent {

    isOpen:boolean = false;


    constructor(){

    };

    toggleNavbar()
    {
        let navbars = document.querySelector('.collapse');
        if(!this.isOpen)
        {
            this.isOpen = !this.isOpen;
            navbars.classList.add('open');
        }
        else
        {
            this.isOpen = !this.isOpen;
            navbars.classList.remove('open');
        }
    }
    
}