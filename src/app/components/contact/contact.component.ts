import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  
  contactForm:object = {
    name: "",
    email: "",
    text: ""
  };

  sent:boolean = false;
  
  constructor() { }

  ngOnInit(){
    
  }

  sendMail(){
    this.sent=true;    
  }
}
