import { Component, OnInit } from '@angular/core';
import { PostService } from '../../../service/post.service'

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  sectionTitle:string = "últimos posts";
  lastThreePosts:Object[] = [];
  constructor(private postsService: PostService ) { }

  ngOnInit() {
    this.lastThreePosts = this.postsService.getPosts();
  }


}
