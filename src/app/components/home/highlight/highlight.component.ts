import { Component, OnInit } from '@angular/core';
import { HighlightserviceService } from '../../../service/highlightservice.service'
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-highlight',
  templateUrl: './highlight.component.html',
  styleUrls: ['./highlight.component.scss']
})
export class HighlightComponent implements OnInit {

  constructor(private highlightService: HighlightserviceService) { }

  subscribeHighlight = new Subscription;
  highlightSelected:Object = this.highlightService.getHighlights();

  ngOnInit() {
    this.subscribeHighlight = this.highlightService.highlightChanged
    .subscribe((highligtSelected:Object) => {
      this.highlightSelected = highligtSelected;
    })
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscribeHighlight.unsubscribe();
  }

}
