import { Component, OnInit } from '@angular/core';
import { PostService } from 'src/app/service/post.service';
import { isUndefined } from 'util';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {

  constructor(private postService: PostService) { }

  allPosts:Object[] = [];
  displayedPosts:Object[] = [];
  currentPost:number = 0;
  loadMorePosts:boolean;

  ngOnInit() {
    this.getPosts();
  }

  getPosts(){
    this.allPosts = this.postService.getPosts();
    (!isUndefined(this.allPosts) && (this.allPosts.length >= 2)) ? this.loadMorePosts = true : this.loadMorePosts = false;
    this.postsToDisplay();
  }

  postsToDisplay(){
    for(var i = 0; i < 2; i++ ){
      if(!isUndefined(this.allPosts[this.currentPost])){ 
        this.displayedPosts.push(this.allPosts[this.currentPost]);
        this.currentPost++;
      }else{
        this.loadMorePosts = false;
      }
    }
    
  }


}
