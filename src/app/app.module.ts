import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule } from '@angular/forms';

import { 
  FooterComponent,
  HeaderComponent
} from './components/shared/layout';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { AppRoutingModule } from './/app-routing.module';
import { HighlightComponent } from './components/home/highlight/highlight.component';
import { PostsComponent } from './components/home/posts/posts.component';
import { IndicatorComponent } from './components/shared/helper/indicator/indicator.component';
import { StreetstyleComponent } from './components/home/streetstyle/streetstyle.component';
import { BlogComponent } from './components/blog/blog.component';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    HighlightComponent,
    PostsComponent,
    IndicatorComponent,
    StreetstyleComponent,
    BlogComponent,
    AboutComponent,
    ContactComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
