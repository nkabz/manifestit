import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  postsArray:Object[] = [
    {src: "../../../../assets/posts/1.jpg", title: "Título da matéria 1", body: "Lorem Lorem", author: "Melissa Janson"},
    {src: "../../../../assets/posts/2.jpg", title: "Título da matéria 2", body: "Lorem Ipsum", author: "Bruna Hirano"},
    {src: "../../../../assets/posts/3.jpg", title: "Título da matéria 3", body: "Donor", author: "Dolab Andrade"}
  ]
  constructor() { }

  getPosts(){
    return this.postsArray.slice(0, 3);
  }
}
