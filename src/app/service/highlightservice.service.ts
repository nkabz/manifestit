import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { isUndefined } from 'util';

@Injectable({
  providedIn: 'root'
})
export class HighlightserviceService {
  highlightsArray = [
    {src: "../../../../assets/highlight/spfw.jpg", title: "Lorem Ipsum"},
    {src: "../../../../assets/highlight/605247-970x600-1.jpeg", title: "Ipsum Donor"},
    {src: "../../../../assets/highlight/desfilefashion.jpg", title: "Donor Lorem"}
  ]

  arrayPosition:number = 0;
  highlightChanged = new Subject<Object>();

  constructor() {
    this.hightLightLoop();
   }
   
  hightLightLoop(){
    setTimeout(() => {
      this.arrayPosition < (this.highlightsArray.length-1) ? this.arrayPosition++ : this.arrayPosition=0;

      this.highlightChanged.next(this.highlightsArray[this.arrayPosition]);

      this.hightLightLoop();
    }, 5000);
  }
  
  getHighlights(){
    return this.highlightsArray[0];

  }


}
